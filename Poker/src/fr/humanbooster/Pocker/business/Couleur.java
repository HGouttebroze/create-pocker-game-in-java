package fr.humanbooster.Pocker.business;

public class Couleur {
	// atribut de la Class (encapsul�, non acc�ssible depuis l'ext)
	private String nom;

	// constructeur : methode qui va permettre de cr�er un objet de type couleur
	// Ce constructeur est le constructeur par d�faut (default constructor)
	// constructeur: m�thode pr cr�� 1 objet de type
	public Couleur() {
		// "super" invoque le constructeur de la clase m�re
		super();
	}

	public Couleur(String nom) {
		// la ligne ci dessous invoque le constructeur par d�fault
		this();
		this.nom = nom;/*
						 * this sert � enlever l'anbiguit� entre le parametre & l'attribut (�
						 * gauche,params & � droite
						 */
		/*
		 * this.nom fait r�f�rence � l'attribut nom. "this" sert � lever l'ambiguit�
		 * entre le param�tre et l'attribut
		 */
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "Couleur [nom=" + nom + "]";
	}
	
	
}

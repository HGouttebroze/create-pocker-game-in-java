package fr.humanbooster.Pocker.business;

import java.util.ArrayList;
//import java.util.Arrays;
import java.util.List;

public class Joueur {

	private String nom;
	private String prenom;
	// Pour d�finir une liste de cartes dans la classe Joueur
	private List<Carte> main;
	private static Long compteur=0L;
	
	public Joueur(String nom, String prenom, List<Carte> main, Long id) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.main = main;
		this.id = id;
	}
	public Joueur() {
		main = new ArrayList<>();
		id=++compteur; // d'abord on incr�mente, puis on affecte le compteur a l'id
	}
	private Long id;
		public Long getId() {
			return id;
		}
	
		public void setId(Long id) {
			this.id = id;
		}
	public Joueur(String prenom) {
		this(null, prenom);
	}

	public Joueur(String nom, String prenom) {
		// super();
		this();
		this.nom = nom;
		this.prenom = prenom;
	}

	
	public Joueur(List<Carte> main) {
		super();
		this.main = main;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public List<Carte> getMain() {
		return main;
	}
	
	public void setMain(List<Carte> main) {
		this.main = main;
	}

	@Override
	public String toString() {
		return "Joueur [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", main=" + main + "]";
	}

	
	
}
 
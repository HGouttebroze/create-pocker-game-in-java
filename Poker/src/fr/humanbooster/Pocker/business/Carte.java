package fr.humanbooster.Pocker.business;

public class Carte {
    private Couleur couleur;
    private String nom;
    private int valeur;
    
    public Carte() {
        // TODO Auto-generated constructor stub
    }
    
    public Carte(Couleur couleur, int valeur) {
        this();
        //Si la valeur est �gale � 11 alors c'est le nom prend la valeur Valet
        //Si la valeur est �gale � 12 alors c'est le nom prend la valeur Dame
        //Si la valeur est �gale � 13 alors c'est le nom prend la valeur Roi
        //Si la valeur est �gale � 14 alors c'est le nom prend la valeur As
        switch (valeur) {
        case 11: 
            nom = "Valet";
            break;
        case 12: 
            nom = "Dame";
            break;
        case 13: 
            nom = "Roi";
            break;
        case 14: 
            nom = "As";
            break;

        default:
            nom = String.valueOf(valeur);
            break;
        }
        this.couleur = couleur;
        this.valeur = valeur;
    }

    public Couleur getCouleur() {
        return couleur;
    }

    public void setCouleur(Couleur couleur) {
        this.couleur = couleur;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getValeur() {
        return valeur;
    }

    public void setValeur(int valeur) {
        this.valeur = valeur;
    }

    @Override
    public String toString() {
        return nom + " de " + couleur.getNom();
    }
    
}
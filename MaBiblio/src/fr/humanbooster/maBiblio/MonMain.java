package fr.humanbooster.maBiblio;

public class MonMain {

	public static void main(String[] args) {
		// on commence par construire ma bibiothèque (la faote est faite volontairement !)
		Bibiotheque bibio = new Bibiotheque();
		// j'ajoute ds la bibio 1 new livre
		bibio.ajouterLivre("Ubik");
		// j'emprunte ce livre
		bibio.emprunterLivre("Ubik");
		// on fait 1 affichage sur la console (sysout+ctrl+espace)
		System.out.println("It's all right!");
	}

}

package fr.humanbooster.maBiblio;

public class Livre { 
	// props
	String nom;
	boolean emprunt;
	
	// operation (on met 1 "void" car l'op� ne rendra rien 
	void empunter() {
		// this est vraiment l'emprunt du livre, il fait r�f�rence � "l'emprunt du livre
		this.emprunt = true;
	}
	
	void rendre () {
		this.emprunt = false;
	}
	
	// pr finir on cr�� le constructeur avec comme params le nom du livre
	Livre (String nom) {
		// on dis k le nom pass� en params est = au nom du livre gr�ce au this
		this.nom = nom;// this.nom est donc le nom du livre et = nom est le nom transmis en params (java le dis qd on le s�lectionne
	}
}
